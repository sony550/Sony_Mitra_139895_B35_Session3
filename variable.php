<?php
$arr=serialize(array("bitm","basis"));

echo $arr;
//var_dump ($arr);
?>

<br>

<?php
$ab ="15bitm";
echo floatval($ab);
?>

<br>

<?php
$var="bitm";
var_dump ($var);
?>

<br>

<?php
$var = 0;

if (empty($var))
{
    echo '$var is either 0, empty, or not set at all';
}
?>

<br>

<?php
$yes = array('this', 'is', 'an array');
echo is_array($yes) ? 'Array' : 'not an Array';
echo "\n";

$no = 'this is a string';
echo is_array($no) ? 'Array' : 'not an Array';
?>

<br>

<?php
error_reporting(E_ALL);
$foo = NULL;
var_dump(is_null($inexistent), is_null($foo));

?>

<br>

<?php
function get_students($obj)
{
    if (!is_object($obj))
    {
        return false;
    }

    return $obj->students;
}
$obj = new stdClass();
$obj->students = array('Kalle', 'Ross', 'Felipe');

var_dump(get_students(null));
var_dump(get_students($obj));
?>

<br>

<?php
$var = '';

if (isset($var))
{
    echo "This var is set so I will print.";
}
$a = "test";
$b = "anothertest";

var_dump(isset($a));
var_dump(isset($a, $b));

unset ($a);
var_dump(isset($a));
var_dump(isset($a, $b));

$foo = NULL;
var_dump(isset($foo));
?>


